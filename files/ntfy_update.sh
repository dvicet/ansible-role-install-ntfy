#!/bin/sh

# - pull server
# - start server (even if it fails)

# usefull for Docker rootless
export DOCKER_HOST=unix:///run/user/$(id --user)/docker.sock

cd $(dirname $0)
export $(cat .env | xargs)

# Update Ntfy
docker compose pull
docker compose up -d
if [ $? -ne 0 ]; then
	curl \
		-H "Authorization: Bearer ${NTFY_INFRA_TOKEN}" \
		-d "ntfy failed to update" https://${NTFY_DOMAIN}/infra
	exit 2
fi
echo "[$(date)]" "Ntfy updated"
